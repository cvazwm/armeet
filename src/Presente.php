<?php
// src/Presente.php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Presente")
 */

class Presente
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $id_curso;
    /**
     * @ORM\Column(type="integer")
     */
    protected $id_sala;
    /**
     * @ORM\Column(type="integer")
     */
    protected $id_participante;
    /**
     * @ORM\Column(type="integer")
     */
    protected $tiempo;

    public function getId()
    {
        return $this->id;
    }

    public function getIdCurso()
    {
        return $this->id_curso;
    }

    public function setIdCurso($id_curso)
    {
        $this->id_curso = $id_curso;
    }
    public function getIdSala()
    {
        return $this->id_sala;
    }

    public function setIdSala($id_sala)
    {
        $this->id_sala = $id_sala;
    }
    public function getIdParticipante()
    {
        return $this->id_participante;
    }

    public function setIdParticipante($id_participante)
    {
        $this->id_participante = $id_participante;
    }
    public function getTiempo()
    {
        return $this->tiempo;
    }

    public function setTiempo($tiempo)
    {
        $this->tiempo = $tiempo;
    }
}