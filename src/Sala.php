<?php
// src/Sala.php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Sala")
 */
class Sala
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $nombre;
    /**
     * @ORM\Column(type="integer")
     */
    protected $curso;
    /**
     * @ORM\Column(type="integer")
     */
    protected $id_creador;
    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $inicio;
    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $fin;
    /**
     * @ORM\Column(type="integer")
     */
    protected $conectados;

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
    public function getCurso()
    {
        return $this->curso;
    }

    public function setCurso($curso)
    {
        $this->curso = $curso;
    }
    public function getIdCreador()
    {
        return $this->id_creador;
    }

    public function setIdCreador($id_creador)
    {
        $this->id_creador = $id_creador;
    }
    public function getInicio()
    {
        return $this->inicio;
    }

    public function setInicio($inicio)
    {
        $this->inicio = $inicio;
    }
    public function getFin()
    {
        return $this->fin;
    }

    public function setFin($fin)
    {
        $this->fin = $fin;
    }
    public function getConectados()
    {
        return $this->conectados;
    }

    public function setConectados($conectados)
    {
        $this->conectados = $conectados;
    }
}