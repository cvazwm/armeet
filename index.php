<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="armeet.ico">

    <style>
        .bd-placeholder-img {
          font-size: 1.125rem;
          text-anchor: middle;
          -webkit-user-select: none;
          -moz-user-select: none;
          user-select: none;
        }
  
        @media (min-width: 768px) {
          .bd-placeholder-img-lg {
            font-size: 3.5rem;
          }
        }
        .oculto{
          display:none;
        }
      </style>
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/signin.css" rel="stylesheet">
    <title>ArMeet - Reuniones sin limites</title>
  </head>
  <body class="text-center">
    

    <main class="form-signin oculto" id="formLogin">
        <form>
          <!-- <img class="mb-4" src="/docs/5.0/assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> -->
          <h1 class="h3 mb-3 fw-normal">Utilizá los datos de tu cuenta de Campus</h1>
          <p>
            <a href="#" class="text-decoration-none" id="lnkComoFunciona">
              ¿Como funciona?
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-square-fill" viewBox="0 0 16 16">
                <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm8.93 4.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
              </svg>
            </a>
          </p>
          <br>
          <div class="form-floating">
            <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
            <label for="floatingInput">Usuario / Email</label>
          </div>
          <div class="form-floating">
            <input type="password" class="form-control" id="floatingPassword" placeholder="Password">
            <label for="floatingPassword">Contraseña</label>
          </div>
      
          <br>
          <button class="w-100 btn btn-lg btn-primary" type="submit" id="btnConectarse">
            Conectarse
          </button>
          <p class="mt-5 mb-3 text-muted">
            <ul class="list-unstyled">
                <li>
                    <ul class="text-start">
                        <li>Este sitio no almacena ninguna información.</li> 
                        <li>Utiliza el servicio de campus para validar el acceso.</li>
                        <li>Los datos personales mostrados son guardados en su pc.</li>
                        <li>La conexión se encuentra cifrada por SSL desde tu navegador hasta el campus.</li>
                    </ul>
                </li>
            </ul>
      <!-- <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p> -->
        </form>

    </main>

    <div class="container oculto" id="salasDeChat">
      <div class="card text-center">
        <div class="card-body">
            <span class="col-12" style="font-weight:bold;">Iniciaste sesión como:</span></br></br>
            
          <div class="row d-flex justify-content-center align-items-center">
            <div class="col-3 col-md-1"><img class="rounded border border-5" src="" id="imagenDePerfil"></div>
            <span class="card-title col-5 col-md-4" id="nombreCompleto" style="margin-left: 10px;font-size:21px;font-weight:bold">
              <button type="button" class="btn mt-2 ml-5 btn-outline-danger" id="btnCerrarSesion">
                Cerrar Sesión
              </button>
            </span>
          </div>
          <p class="card-text"></p>
        </br>
          <a href="#" class="btn btn-primary" id="btnCrearSala">Crear una sala</a></br>
        </div>
      </div>

      </br></br>
      <h2 class="pb-2 border-bottom">
        Salas de video activas
      </h2>
    
      <?php
        if(isset($_COOKIE['sesionPHP'])){
          echo '<div id="sesion" class="oculto">' .$_COOKIE['sesionPHP']. '</div>';
          echo '<div id="tknunp" class="oculto">' .$_COOKIE['nunp']. '</div>';
        }else{
          echo '<div id="sesion" class="oculto"></div>';
          echo '<div id="tknunp" class="oculto"></div>';
        };
      ?>

      <div class="row py-5" id="listaDeSalas"> <!-- align-items-stretch -->

        <!-- <div class="col">
          <div class="card card-cover overflow-hidden mb-4  text-white bg-dark rounded-5 shadow-lg" style="background-image: url('unsplash-photo-1.jpg');">
            <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
              <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Short title, long jacket</h2>
              <ul class="d-flex list-unstyled mt-auto">
                <li class="me-auto">
                  <img src="https://github.com/twbs.png" alt="Bootstrap" width="32" height="32" class="rounded-circle border border-white">
                </li>
                <li class="d-flex align-items-center me-3">
                  <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"></use></svg>
                  <small>Earth</small>
                </li>
                <li class="d-flex align-items-center">
                  <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"></use></svg>
                  <small>3d</small>
                </li>
              </ul>
            </div>
          </div>
        </div> -->
    
      </div>

      <div class="alert alert-warning oculto mt-5" id="warningNoHaySalas" role="alert">
        No hay ninguna sala disponible.
      </div>
    </div>
      
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src='js/jitsi.js'></script>
    <script>
        $(document).ready(function(){          

          //Verificar si existe una sesion iniciada
          if($('#sesion').html() == ""){
            $('#formLogin').fadeIn();
          }else{
            var nombre = JSON.parse($('#tknunp').html()).name;
            var email = JSON.parse($('#tknunp').html()).email;
            var dni_numero = JSON.parse($('#tknunp').html()).dni_numero;
            var img_perfil = JSON.parse($('#tknunp').html()).img_perfil;
            $('#salasDeChat').fadeIn();
            $('#imagenDePerfil').attr('src', img_perfil);
            $('#nombreCompleto').html( nombre + ' <button type="button" class="btn mt-2 btn-outline-danger" id="btnCerrarSesion">Cerrar Sesión</button>');

            actualizarParticipante("inicio");
            actualizarLista();
          }

          $('#lnkComoFunciona').click(function(){
            alert('como funciona?');
          });


          $('#btnConectarse').click(function(){
              event.preventDefault();

            //Verificar que ambos campos esten completos antes de continuar     !!!
            if($('#floatingInput').val()!="" && $('#floatingPassword').val()!=""){
              $('#btnConectarse').html('<span class="spinner-border spinner-border-sm mb-1 me-2" role="status" aria-hidden="true"></span>');
              $('#btnConectarse').prop('disabled', true);
              $.post( "login.php", { usuario: $('#floatingInput').val(), contraseña: $('#floatingPassword').val() })
              .done(function( data ) {
                usuario = JSON.parse(data);
                if(usuario.name!=""){
                  //Luego de obtener los datos y verificar que el nombre devuelva un valor racional, se graba la sesion.
                  $.post( "validado.php", { vasr: data })
                  .done(function( data2 ) {
                    if(data2 == "ok"){
                      $('#imagenDePerfil').attr('src', usuario.img_perfil);
                      $('#nombreCompleto').html(usuario.name + ' <button type="button" class="btn mt-2 btn-outline-danger" id="btnCerrarSesion">Cerrar Sesión</button>');
                      $('#formLogin').hide();
                      $('#salasDeChat').fadeIn();
                      actualizarParticipante("registro");
                    }else{
                      alert('Hubo un error al intentar guardar temporalmente los datos en tu navegador, solicita ayuda');
                      $('#btnConectarse').html('Conectarse');
                      $('#btnConectarse').prop('disabled', false);
                    }
                  });
                }else{
                  alert('Datos incorrectos, intenta de nuevo.');
                  $('#btnConectarse').html('Conectarse');
                  $('#btnConectarse').prop('disabled', false);
                }
                /*  console.log(usuario.name);
                console.log(usuario.dni_numero);
                console.log(usuario.email);
                console.log(usuario.img_perfil); */

              },"json");
            }else{
              alert('Debes ingresar ambos datos de acceso.');
              $('#btnConectarse').html('Conectarse');
              $('#btnConectarse').prop('disabled', false);
            }
          });

          $('#btnCerrarSesion').click(function(){
            $.post( "logout.php", function( data ) {
              if(data=="ok"){
                $('#imagenDePerfil').attr('src', "");
                  $('#nombreCompleto').html("");
                  $('#formLogin').fadeIn();
                  $('#salasDeChat').hide();
                  $('#btnConectarse').html('Conectarse');
                  $('#btnConectarse').prop('disabled', false);
              };
            });
          });

          $('#btnCrearSala').click(function(){
            console.log('crear sala');
          });

        });

        function actualizarLista(){
            //Consulto en MYsql si hay alguna sala abierta con un POST y AJAX
                if($("#listaDeSalas .col").length==0){
                    $('#listaDeSalas').hide();
                    $('#warningNoHaySalas').fadeIn();
                }else{
                    $('#listaDeSalas').fadeIn();
                    $('#warningNoHaySalas').hide();
                }
            }
        
        function actualizarParticipante(entorno){
          //Busco la ip del cliente (al ser un proceso asincronico se debe realizar antes de proseguir)
          $.getJSON("https://api.ipify.org/?format=json", function(e) {

                //grabar en tabla participante la ultima ip en el identificador correspondiente.
                //a esta altura se puede utilizar los datos en cookies o los grabados en el dom con javascript
                if(entorno=="inicio"){
                  //realizar post con "e.ip"
                  //inicio solo actualiza la ip en el entity participante
                }else if (entorno=="registro"){
                  //realizar post con datos de nombre y "e.ip"
                  //registro crea una entrada en el entity participante, con la ip inicial
                }
          });
          
        }

    </script>
  </body>
</html>