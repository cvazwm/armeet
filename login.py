import requests
import json
import sys

if (len(sys.argv) < 2):
    user = input('usuario: ')
    password = input('contraseña: ')
else:
    user = str(sys.argv[1])
    password = str(sys.argv[2])

#CORROBORAR QUE NO ESTE INICIADA LA SESION SINO REALIZAR ESTO:
curSession = requests.Session() #ALMACENO TODAS LAS COOKIES DEL SITIO PARA UN INICIO EXITOSO
urlInicio = 'https://campus.unla.edu.ar/aulas/login/index.php'
#INTENTO OBTENER TOKEN
xInicio = curSession.post(urlInicio)
primer_extracto = xInicio.text[xInicio.text.find('logintoken')+40:xInicio.text.find('logintoken')+200]
tokenSesion = primer_extracto[primer_extracto.find('"')+1:primer_extracto.find('"',primer_extracto.find('"')+1)]

#INGRESO LOS DATOS DE INICIO DE SESION
urlLogin = 'https://campus.unla.edu.ar/aulas/login/index.php'
logindata = {'username': user, 'password': password, 'logintoken': tokenSesion}
#REALIZO EL INTENTO DE INICIO DE SESION
inicioHTMLCompleto = curSession.post(urlLogin, data = logindata)

nombre = inicioHTMLCompleto.text[inicioHTMLCompleto.text.find('id="usermenu"'):inicioHTMLCompleto.text.find('id="usermenu"')+400]
nombre_completo = nombre[nombre.find('title="')+7:nombre.find('"',nombre.find('title="')+8)]
img_perfil = nombre[nombre.find('https'):nombre.find('"',nombre.find('f1?rev=')+8)]
#print(nombre_completo)
#print(img_perfil)

urlProfile = 'https://campus.unla.edu.ar/aulas/user/edit.php'
profileHTMLCompleto = curSession.post(urlProfile)
dni_numero = profileHTMLCompleto.text[profileHTMLCompleto.text.find('"',profileHTMLCompleto.text.find('id="id_idnumber"')+17)+1:profileHTMLCompleto.text.find('"',profileHTMLCompleto.text.find('"',profileHTMLCompleto.text.find('id="id_idnumber"')+17)+1)]
email_perfil = profileHTMLCompleto.text[profileHTMLCompleto.text.find('"',profileHTMLCompleto.text.find('id="id_email"')+14)+1:profileHTMLCompleto.text.find('"',profileHTMLCompleto.text.find('"',profileHTMLCompleto.text.find('id="id_email"')+14)+1)]
#print(dni_numero)
#print(email_perfil)
datos = '{"name":"' + nombre_completo + '","email":"' + email_perfil + '","dni_numero":"' + dni_numero + '","img_perfil":"' + img_perfil + '"}'
print (datos) #si se parsea en JSON al llegar al javsacript emite error que no reconoce el caracter ', sin codificar en JSON funciona correctamente